var fs = require('fs');
var path = require('path');
var dirPath = path.resolve(__dirname); // path to your directory goes here
var filesList;
fs.readdir(dirPath, function(err, files){
  filesList = files.filter(function(e){
    return path.extname(e).toLowerCase() === '.rmd'
  });

    //filesList = filesList.reverse();
    var id = 4;
    var filesArray = [];
    var colabsArray = [];
    var docsArray = [];
  //console.log(filesList);
    //
    let googlecolabidsdata = fs.readFileSync('google-colab-ids.json');
    let googlecolabids = JSON.parse(googlecolabidsdata);

    for(f of filesList){
      var parsed = path.parse(f);
      var name = parsed.name;

      var htmlFile = name + '.html'
      var rmdFile = name + '.Rmd'
      var ipynbFile = name + '.ipynb'
	
      var googlecolabId = googlecolabids[name]; 

	var fileItem = {
	  id: id++,
	  name: name,
	  children: [
	    { id: id++, name: rmdFile, file: 'Rmd' },
	    { id: id++, name: htmlFile, file: 'html' },
	    { id: id++, name: ipynbFile, file: 'ipynb'}
	  ]
	};

	filesArray.push(fileItem);

	var colabItem = {
	     id: id++, 
	     name: name, 
	     file: 'colab', 
	     googleid: googlecolabId,
	};
	colabsArray.push(colabItem);
    }

    let myJson = [ 
	{ 
	    id: 1,
	    name: "Files",
	    children: filesArray 
	},
	{
	    id: 2,
	    name: "Colab Links",
	    children: colabsArray 
	}
    ];
		    
    console.log(JSON.stringify(myJson, null, 2));
});
