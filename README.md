This project includes different rmarkdown machine learning documents for the youtube Jalayer videos. [Jalayer youtube videos](https://www.youtube.com/watch?v=RIFQ2nUDsJI&list=PLjPbBibKHH18I0mDb_H4uP3egypHIsvMn) 

The colab notebooks are meant to be used by students watching the videos and completing the notebooks as you follow along. They insert code and insert markdown to explain what is going on with the code. Not all videos have Colab notebooks though since there is not a need to do things like install R when using Colab.

You can see the rendered versions of the files at the pages site for this repo:
[Rmarkdown Jalayer Videos Pages](https://calvinw.gitlab.io/rmarkdown-jalayer-videos)

This project is structured in a similar way to how the projects [Rmarkdown Colab Google Docs Project](https://gitlab.com/calvinw/rmarkdown-colab-google-docs) and [Machine Learning Rmarkdown Project](https://gitlab.com/calvinw/machine-learning-rmarkdown) are so you can look there for more details on how the build process works.  

Basically each Rmd file can be rendered to the following formats: 
1. html
1. ipynb 

The ipynb files are uploaded to google drive as Colab notebooks. You can see more about Gooogle Colab [here](https://colab.research.google.com/) Each one specifies the kernel (R) for Colab to use.  

These ipynb files are created by a command line tool called [jupytext](https://github.com/mwouts/jupytext).  

Jupytext allows converting from rmarkdown or (regular) markdown to ipynb formats.

We use `rmarkdown::render` in our Makefile to render the html and md versions of our files, then `jupytext` to render from md to ipynb.

[Jupyter](https://jupyter.org/) must be installed as well since it is used to create the R kernel version of the ipynb formats. Also the R kernel of Jupyter is used in the R versions of the ipynb Colab formats. This kernel choice works in Google Colab, though it is not advertised yet.   

To see what the project dependencies look like, take a look at the .gitlab-ci.yml file since that is a recipe to install pre-reqs for this project. Roughly the relevant things to install are as follows:

We just show installing these from the command line. If you are in RStudio you can use the Terminal window there. 

Here roughly is what is needed

```bash
apt-get -y install libcurl4-openssl-dev libxml2-dev libssl-dev pandoc python3-pip
pip3 install jupyter jupytext
wget -qO- "https://yihui.name/gh/tinytex/tools/install-unx.sh" | sh
  - Rscript -e "install.packages('rmarkdown')"
  - Rscript -e "install.packages('rvest')"
  - Rscript -e "install.packages('RCurl')"
  - Rscript -e "install.packages('C50')"
  - Rscript -e "install.packages('rpart')"
  - Rscript -e "install.packages('reticulate')"
  - Rscript -e "install.packages('IRkernel')"
  - Rscript -e "IRkernel::installspec()"
```

Then from the command line build it this way: 

```bash
make 
```

or you can use Rstudio and choose the "Build All" menu from the Build tab. Likely if you just choose "Build All" in RStudio it will complain and make you install all the pre-reqs above as you go along. This is fine, just enter the above in RStudios Terminal window as you go along.

The above should be enough to get the files built. If you would like to investigate uploading the files to Google Colab or Google Drive, or if you would like to run the nodejs server we have in this repo, you have to install nodejs and npm. We don't cover installing these, just google them if you need to). You will then have to run: 

```bash
npm install
```

which uses the package.json file in this directory.

We use a node app called google-upload.js to upload the built .ipynb to Google Colab (really just Google Drive) 

If you would like to make it so that you can automatically upload the ipynb and docx files to your Google Colab folder you will have to create a credentials.json file and place it in this directory. Go to the googles developer console, and create one in a similar fashion to this document from google:  

[Google Drive API Quickstart](https://developers.google.com/drive/api/v3/quickstart/nodejs)

This is basically the example we began with that became our google-upload.js. You will need to create a credentials.json like they do in that example. When you run google-upload.js it will upload to your versions of the Google Colab notebooks into your google drive.

Before you run the google-upload.js, you should create a Google Colab file for each Rmd you create You can "make a copy" of one of the current files in [Rmarkdown for Jalayer Videos Google Folder](https://drive.google.com/open?id=1xUJHZfx7BlhNv57Jaf6m-zhmheO2GGAq). Then you note its googleid.

Then add the new googleid for the new file to google-colab-ids.json.

Note google-upload.js app does not create any Google Docs for you, just uploads and saves to existing ones after they are rendered by the make process.

Once you have the google ids you will use, you can upload the current built versions of the your new ipynb like this:

```bash
node google-upload.js Blah.ipynb
```

This reads the googleid for the Colab document Blah from google-colab-ids.json and uploads the Blah.ipynb to the file in your google drive. 

When you run this you will ask you to authenticate the app and allow it to access your Google Drive. For this it uses the credentials.json you made above. It creates a token.json as you do this and that will be used on subsequent runs of the tool. This is the same process that the quickstart example from google above uses, so refer to that if you need more info on this process.. 

The files we use in google drive are at this link: 

[Rmarkdown for Jalayer Videos Google Drive Folder](https://drive.google.com/open?id=1xUJHZfx7BlhNv57Jaf6m-zhmheO2GGAq)

These should be shared with you, so you can take a look.

See the Makefile for additional build details and targets.
