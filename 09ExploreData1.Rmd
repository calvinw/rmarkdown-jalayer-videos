This notebook accompanies the video here: 

[R - Exploring Data (part 1) - Import Data in R](https://www.youtube.com/watch?v=dJEhINzZOaw&list=PLjPbBibKHH18I0mDb_H4uP3egypHIsvMn&index=9)

Instead of reading from the local computer (like in the video), this notebook reads the file from online and then loads it in to a data frame after that. In order to do that we first load the RCurl package which allows to load text files from remote web site URLs. The video shows loading the data file from a local directory (folder), which is diffent from what we do in this notebook.

First make sure RCurl is installed.

```{r eval=FALSE}
install.packages("RCurl")
```

Then make sure it is loaded:

```{r}
library("RCurl")
```

We use the getURL function from the RCurl package to load the data from the website. It is returned to us as a text string. 

```{r}
textString<-getURL("https://raw.githubusercontent.com/stedy/Machine-Learning-with-R-datasets/master/usedcars.csv")
```

Now we use the following code ( instead of code `df3<-read.csv("usedcars.csv)"` as shown in the video):

```{r}
df3<-read.csv(text=textString)
```

The result of this is that the data frame `df3` should now look exactly like the data frame in the video from here onwards.

```{r include=FALSE, eval=TRUE}
knitr::knit_exit()
```

```{r}
str(df3)
```

```{r}
head(df3)
```

```{r}
df4<-read.csv(text=textString, stringsAsFactors=FALSE)
str(df4)
```

```{r}
head(df4)
```
